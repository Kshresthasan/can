<section class="bg-primary">
	<!-- <article class="py-5 ">
		<div class="text-center text-white">
			<h3 class="display-5 ">Want to join?</h3>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
			<button class="btn btn-warning"data-toggle="modal" data-target="#myModal">Join now</button>
		</div> -->

		<!-- The Modal -->
  <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Sign up</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
			 <form action="/action_page.php">
					  <div class="form-group">
					    <label for="email">Email address:</label>
					    <input type="email" class="form-control" id="email" autocomplete="off">
					  </div>
					  <div class="form-group">
					    <label for="pwd">Password:</label>
					    <input type="password" class="form-control" id="pwd">
					  </div>
					  <div class="form-group form-check">
					    <label class="form-check-label">
					      <input class="form-check-input" type="checkbox"> Remember me </label>
					  </div>
					  <button type="submit" class="btn btn-primary">Submit</button>
				</form> 
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
  
	<!-- </article> -->
</section>