<?php include_once('header.php'); ?>


<section class="banner-wrapper">
    <div class="container">
        <div class="banner-items image-center col-10 mx-auto">
            <img src="images\pabson-banner.jpg">
        </div>
    </div>
</section>


<section class="content-wrapper section-padding online-boking">
    <div class="container">
        <div class="booking-caption col-10 mx-auto my-5">
            <h1 class="online-boking-bg">Online Stall Booking</h1>
        </div>
         <h4 class="text-center my-2">select Stall type</h4>
        <div class="col-10 mx-auto">
            
            <div>
                    <select data-toggle="modal" data-target="#pbtn" class="form-control ng-pristine ng-valid ng-empty ng-touched" ng-model="filter.type" ng-change="pageChanged(1)" ng-options="item as stall_type_display(item) for item in stall_type_list" style="">
                        <option value="" class="" selected="selected">----</option>
                        
                        <option label="Pavillions(5m X 6m)(24 Pavillions)  (Rs. 130,000)" value="string:Pavillions ">(5m X 6m)(24 Pavillions)  (Rs. 130,000)</option>
                        <option label="Stalls(3m X 3m)(88 Stalls)  (Rs. 40,000)" value="string:stalls ">(3m X 3m)(88 Stalls)  (Rs. 40,000)</option>
                        <option label="Stalls(3m X 2m)(44 Stalls)  (Rs. 25,000)" value="string:stalls ">(3m X 2m)(44 Stalls)  (Rs. 25,000)</option>
                        <option label="Pavillions(30ft X 20ft)(1 Pavillions)  (Rs. 150,000)" value="string:Pavillions ">(30ft X 20ft)(1 Pavillions)  (Rs. 150,000)</option>
                        <option label="Stalls(3m X 3m)(24 Stalls)  (Rs. 130,000)" value="string:stalls">(3m X 3m)(24 Stalls)  (Rs. 130,000)</option>
                        </select>
                </div>
        </div>
    </div>
</section>

<?php include_once('pbtn.php'); ?>



<?php include_once('footer.php'); ?>
